// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTSOULS_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
#pragma region Params
	bool bEnemyHealthBarVisible;

	FVector EnemyLocation;
#pragma endregion

#pragma region HUD
	// Reference to the UMG asset in the editor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> HUDOverlayAsset;

	// Variable to hold the widget after creating it
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		UUserWidget* HUDOverlay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<UUserWidget> wEnemyHealthBar;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Widgets")
		UUserWidget* EnemyHealthBar;
#pragma endregion

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

public:

#pragma region HUD Methods
	void DisplayEnemyHealthBar();
	void RemoveEnemyHealthBar();
#pragma endregion

};
