// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Normal		UMETA(DisplayName = "Normal"),
	EMS_Sprinting	UMETA(DisplayName = "Sprinting"),
	EMS_Dead		UMETA(DisplayName = "Dead"),

	EMS_MAX			UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EStaminaStatus : uint8
{
	ESS_Normal				UMETA(DisplayName = "Normal"),
	ESS_BelowMinimum		UMETA(DisplayName = "BelowMinimum"),
	ESS_Exhausted			UMETA(DisplayName = "Exhausted"),
	ESS_ExhaustedRecovering UMETA(DisplayName = "ExhaustedRecovering"),

	ESS_MAX					UMETA(DisplayName = "DefaultMax")
};

UCLASS(config=Game)
class PROJECTSOULS_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

#pragma region Player Stats
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Stats")
		float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
		float Health;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Stats")
		float MaxStamina;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
		float Stamina;
#pragma endregion

#pragma region HUD
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Controller")
		class AMainPlayerController* MainPlayerController;
#pragma endregion

#pragma region Character Movement
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Enums")
		EMovementStatus MovementStatus;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Enums")
		EStaminaStatus StaminaStatus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaDrainRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MinSprintStamina;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Running")
		float RunningSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Running")
		float SprintingSpeed;

	bool bMovingForward;
	bool bMovingRight;	
#pragma endregion

#pragma region DSCamera
	// Camera boom positioning the camera behind the character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class ULockArmComponent* CameraLockArm;

	// Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	// Target component for camera lock on system
	class UTargetComponent* TargetComponent;

	// Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Lock On Camera")
		float BaseTurnRate;

	// Base look up / down rate, in deg / sec.Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Lock On Camera")
		float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Lock On Camera")
		float LockonControlRotationRate;

	// Tolerance for a mouse movement to be considered an input to switch targets
	UPROPERTY(EditDefaultsOnly, Category = "Lock On Camera")
		float TargetSwitchMouseDelta;

	// Tolerance for a mouse movement to be considered an input to switch targets
	UPROPERTY(EditDefaultsOnly, Category = "Lock On Camera")
		float TargetSwitchAnalogValue;

	// True if player returned analog-stick to center after last target switch
	bool bAnalogSettledSinceLastTargetSwitch;

	// Cooldown time before another target switch can occur, used to make target switching more controllable
	UPROPERTY(EditDefaultsOnly, Category = "Lock On Camera")
		float TargetSwitchMinDelaySeconds;

	// Time that last target switch occurred
	UPROPERTY(BlueprintReadOnly, Category = "Lock On Camera")
		float LastTargetSwitchTime;

	// Tolerance for a mouse movement to be considered an input to break target lock
	UPROPERTY(EditDefaultsOnly, Category = "Lock On Camera")
		float BreakLockMouseDelta;

	// Time to wait after breaking lock with mouse movement before player gets control of the camera back.  Prevents over scrolling camera after breaking lock.
	UPROPERTY(EditDefaultsOnly, Category = "Lock On Camera")
		float BrokeLockAimingCooldown;

	// Time that player broke camera lock at
	float BrokeLockTime;
#pragma endregion


#pragma region Controls
	bool bShiftKeyDown;
	bool bLMBDown;
#pragma endregion

#pragma region Camera
	// Camera boom positioning the camera behind the player
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
#pragma endregion

#pragma region Items
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items)
		class AItem* ActiveOverlappingItem;
#pragma endregion

#pragma region Equipment
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Items)
		class AWeapon* EquippedWeapon;
#pragma endregion

#pragma region Combat
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Anims")
		bool bAttacking;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
		bool bHasCombatTarget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
		class AEnemy* CombatTarget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
		TSubclassOf<AEnemy> EnemyFilter;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
		FVector CombatTargetLocation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Anims")
		class UAnimMontage* CombatMontage;

#pragma endregion

#pragma region Sound Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
		class USoundCue* HitSound;
#pragma endregion


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

#pragma region DSCamera
	// Handle horizontal mouse input
	void Turn(float Val);

	// Handle vertical mouse input
	void LookUp(float Val);

		// Tick every frame;
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
#pragma endregion


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

#pragma region Character Movement Methods

	// Called for forwards/backwards input
	void MoveForward(float Value);

	// Called for side to side input
	void MoveRight(float Value);

	bool CanMove(float Value);

	// Set movement status and running speed
	void SetMovementStatus(EMovementStatus status);

	void Dodge();

	FORCEINLINE void SetStaminaStatus(EStaminaStatus Status) { StaminaStatus = Status; }
#pragma endregion

#pragma region Controls Methods
	// Pressed down to enable sprinting
	FORCEINLINE void ShiftKeyDown() { bShiftKeyDown = true; }

	// Released to stop sprinting
	FORCEINLINE void ShiftKeyUp() { bShiftKeyDown = false; }

	void LMBDown();

	FORCEINLINE void LMBUp() { bLMBDown = false; }
#pragma endregion

#pragma region DSCamera
	// Returns CameraBoom subobject
	UFUNCTION(BlueprintCallable, Category = "Lock On Camera")
		FORCEINLINE class ULockArmComponent* GetCameraBoom() const { return CameraLockArm; }

	// Returns FollowCamera subobject
	UFUNCTION(BlueprintCallable, Category = "Lock On Camera")
		FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
#pragma endregion

#pragma region Camera Methods
	/* Called via input to turn at a given rate
	* @param Rate - This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/* Called via input to look up/down at a given rate
	* @param Rate - This is a normalized rate, i.e. 1.0 means 100% of desired look up/down rate
	*/
	void LookUpAtRate(float Rate);
#pragma endregion

#pragma region Item Methods
	FORCEINLINE void SetActiveOverlappingItem(AItem* Item) { ActiveOverlappingItem = Item; }
#pragma endregion

#pragma region Equipment Methods
	void SetEquippedWeapon(AWeapon* WeaponToSet);

	FORCEINLINE AWeapon* GetEquippedWeapon() { return EquippedWeapon; }
#pragma endregion

#pragma region Combat Methods
	void Attack();

	UFUNCTION(BlueprintCallable)
		void AttackEnd();

	FORCEINLINE void SetHasCombatTarget(bool HasTarget) { bHasCombatTarget = HasTarget; }
	FORCEINLINE void SetCombatTarget(AEnemy* Target) { CombatTarget = Target; }

	void UpdateCombatTarget();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void Die();

	UFUNCTION(BlueprintCallable)
	void DeathEnd();
#pragma endregion

#pragma region Sound Methods
	UFUNCTION(BlueprintCallable)
		void PlaySwingSound();
#pragma endregion


};
