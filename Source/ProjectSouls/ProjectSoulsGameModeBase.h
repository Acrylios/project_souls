// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectSoulsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTSOULS_API AProjectSoulsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
