// Fill out your copyright notice in the Description page of Project Settings.


#include "TargetComponent.h"

/**
* Targetable component used for camera lock-on system
* For selection only.  Intentionally empty.
*/